<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

require('Employee.php');
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

$postdata = file_get_contents('php://input');
$obj = new Employee();
$data = $obj->checkEmailExists($postdata);
echo $data;
?>