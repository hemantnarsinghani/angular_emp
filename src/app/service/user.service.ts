import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {User} from "../model/user.model";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
    baseUrl: string = 'http://localhost/angular_api2/';

	  getusers() {
	    return this.http.get<User[]>(this.baseUrl+'select.php');
	  }

	  saveUser(user:User){
	  	return this.http.post(this.baseUrl+'add.php',user);
	  }

	  getUserById(id){
	  	return this.http.post(this.baseUrl+'selectbyid.php',id);
	  }


	  updateUser(user:User){
	  	return this.http.post(this.baseUrl+'edit.php',user);
	  }

	  deleteUser(id){
	  	return this.http.post(this.baseUrl+'delete.php',id);
	  }

	  isEmailRegisterd(email){
	  	return this.http.post(this.baseUrl+'checkemailexists.php',email);
	  }

}
