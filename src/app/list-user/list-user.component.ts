import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../service/user.service";
import {User} from "../model/user.model";
import { NgFlashMessageService } from 'ng-flash-messages';





@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  constructor(private router: Router,private userService: UserService,private flashmessage: NgFlashMessageService) { }

   users: User[];


  ngOnInit() {
  	this.userService.getusers().subscribe( data => {
        this.users = data;
      });
  }

  addUser(){
  	this.router.navigate(['add-user']);
  }

  editUser(id){
    this.router.navigate(['edit-user/'+id]);
  }

  deleteUser(user:User){
    this.userService.deleteUser(user.id).subscribe(data=>{
      if(data==1){
        this.flashmessage.showFlashMessage({
          // Array of messages each will be displayed in new line
          messages: ["Successfully deleted user"], 
          // Whether the flash can be dismissed by the user defaults to false
          dismissible: true, 
          // Time after which the flash disappears defaults to 2000ms
          timeout: false,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'success'
        });
      }else{
        this.flashmessage.showFlashMessage({
          // Array of messages each will be displayed in new line
          messages: ["Something went to wrong please try again."], 
          // Whether the flash can be dismissed by the user defaults to false
          dismissible: true, 
          // Time after which the flash disappears defaults to 2000ms
          timeout: false,
          // Type of flash message, it defaults to info and success, warning, danger types can also be used
          type: 'danger'
        });
      }
     this.users = this.users.filter(u => u !== user);
    });
  }

}
