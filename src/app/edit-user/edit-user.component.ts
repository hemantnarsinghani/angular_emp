import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../service/user.service";
import {User} from "../model/user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import { NgFlashMessageService } from 'ng-flash-messages';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  editUserForm: FormGroup;
  formSubmited:boolean = false;
  constructor(private router: Router,private formBuilder: FormBuilder,private route: ActivatedRoute,private userservice:UserService,private flashmessage: NgFlashMessageService) {}

  ngOnInit() {

  	this.editUserForm = this.formBuilder.group({
      email_id: ['', [Validators.required,Validators.email]],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      id: ['', Validators.required]
    });

  	let userId = this.route.snapshot.paramMap.get('id');

  		
	  	this.userservice.getUserById(userId).subscribe(data=>{
	  		 this.editUserForm.setValue(data);
	  	});
  }

  onSubmit(){
  	this.formSubmited  = true;
  	if (this.editUserForm.invalid) {
	      return 1;
	}
  	let formData = this.editUserForm.value;

  	this.userservice.updateUser(formData).subscribe(data=>{
  		if(data==1){
  			this.flashmessage.showFlashMessage({
		      // Array of messages each will be displayed in new line
		      messages: ["Successfully added user"], 
		      // Whether the flash can be dismissed by the user defaults to false
		      dismissible: true, 
		      // Time after which the flash disappears defaults to 2000ms
		      timeout: false,
		      // Type of flash message, it defaults to info and success, warning, danger types can also be used
		      type: 'success'
		    });
  		}else{
  			this.flashmessage.showFlashMessage({
		      // Array of messages each will be displayed in new line
		      messages: ["Something went to wrong please try again."], 
		      // Whether the flash can be dismissed by the user defaults to false
		      dismissible: true, 
		      // Time after which the flash disappears defaults to 2000ms
		      timeout: false,
		      // Type of flash message, it defaults to info and success, warning, danger types can also be used
		      type: 'danger'
		    });
  		}
  		this.router.navigate(['list-user']);
  	});

  }
}
