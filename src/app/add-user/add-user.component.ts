import { Component, OnInit } from '@angular/core';
import {UserService} from "../service/user.service";
import {User} from "../model/user.model";
import {FormBuilder, FormGroup, Validators,FormControl} from "@angular/forms";
import {Router,Resolve} from "@angular/router";
import { NgFlashMessageService } from 'ng-flash-messages';




@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private router: Router,private formBuilder: FormBuilder,private userService: UserService,private flashmessage: NgFlashMessageService) { }
  addUserForm: FormGroup;
  submitted: boolean = false;


  ngOnInit() {
	this.addUserForm = this.formBuilder.group({
      email_id: ['', [Validators.required,Validators.email],this.isEmailUnique.bind(this)],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required]
    });
  }
			  
    isEmailUnique(control: FormControl) {
      const q = new Promise((resolve, reject) => {
        setTimeout(() => {
         this.userService.isEmailRegisterd(control.value).subscribe(() => {
           resolve(null);
         }, () => { resolve({ 'isEmailUnique': true }); });
       }, 1000);
     });
     return q;
    }



  onSubmit(){
	this.submitted  = true;
	if (this.addUserForm.invalid) {
	      return 1;
	}
	let formData = this.addUserForm.value;
	this.userService.saveUser(formData).subscribe(data=>{
		if(data==1){
			this.flashmessage.showFlashMessage({
		      // Array of messages each will be displayed in new line
		      messages: ["Successfully added user"], 
		      // Whether the flash can be dismissed by the user defaults to false
		      dismissible: true, 
		      // Time after which the flash disappears defaults to 2000ms
		      timeout: false,
		      // Type of flash message, it defaults to info and success, warning, danger types can also be used
		      type: 'success'
		    });
		}else{
			this.flashmessage.showFlashMessage({
		      // Array of messages each will be displayed in new line
		      messages: ["Something went to wrong please try again."], 
		      // Whether the flash can be dismissed by the user defaults to false
		      dismissible: true, 
		      // Time after which the flash disappears defaults to 2000ms
		      timeout: false,
		      // Type of flash message, it defaults to info and success, warning, danger types can also be used
		      type: 'danger'
		    });
		}
		this.router.navigate(['list-user']);
	});
  }

}
